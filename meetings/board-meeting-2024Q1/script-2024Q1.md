Board Meeting Script 2024Q1

## Preamble

President: Welcome to the Redox OS board meeting for Q1 of 2024.

## Call to order

President. I call the meeting of the Board of Directors of Redox OS to order.

## Attendance

Ron: When I state your name please acknowledge your presence.

- Ron Williams: Present
- Jeremy Soller: Present
- Alberto Souza: Present

Ron: We have a quorum.

We also have as a guest Jacob Schneider.

- Jacob: Hello.

## Minutes from last meeting

Ron: The minutes from the previous board meeting have been provided to you.
Are there any corrections?

Ron: If there are no corrections...

Ron: The minutes are approved as distributed.

## Elections of Directors

Ron: As required by the Bylaws, the Redox OS nonprofit elects its directors and officers each year. Just to clarify, an officer is someone who performs duties on behalf of the board, while a director is a voting member of the board.

Redox officers are also directors, so as we elect our officers, we are also electing them as members of the board. In particular, the president presides over board meetings and the vice-president acts in the president's place if the president cannot attend a board meeting.

I would like to open the floor to nominees for president and director.

Jeremy: I nominate Ronald Williams to the office of president and director.

Ron: Are there any other nominees for president and director?

Ron: If there are no other nominees...

Ron: All those in favor of myself, Ronald Williams, for president and director, say Aye, those opposed say No.

Jeremy: Aye.

Alberto: Aye.

Ron: The Ayes have it, yours truly, Ronald Williams, is elected president and director.

Ron: I would like to open the floor to nominees for vice-president and director.

Alberto: I nominate Jeremy Soller to the office of vice-president and director.

Ron: Are there any other nominees for vice-president and director?

Ron: If there are no other nominees...

Ron: All those in favor of Jeremy Soller for vice-president and director say Aye, those opposed say No.

Alberto: Aye.

Ron: Aye.

Ron: The Ayes have it, Jeremy Soller is elected vice-president and director.

Ron: I would like to open the floor to nominees for secretary and director.

Jeremy: I nominate Alberto Souza as secretary and director.

Ron: If there are no other nominees...

Ron: All those in favor of Alberto Souza for secretary and director say Aye, those opposed say No.

Jeremy: Aye.

Ron: Aye.

Ron: The Ayes have it, Alberto Souza is elected secretary and director.

Ron: I would like to open the floor to nominees for treasurer.

Alberto: I nominate Jeremy Soller for treasurer.

Ron: If there are no other nominees...

Ron: All those in favor of Jeremy Soller for treasurer say Aye, those opposed say No.

Alberto: Aye.

Ron: Aye.

Ron: The Ayes have it, Jeremy Soller is elected treasurer.

Ron: I would like to open the floor to nominees for other directors.

Jeremy: I nominate Jacob Schneider for director.

Ron: If there are no other nominees...

Ron: All those in favor of Jacob Schneider for director say Aye, those opposed say No.

Jeremy: Aye.

Alberto: Aye.

Ron: Aye.

Ron: The Ayes have it, Jacob Schneider is elected director.

Ron: Jacob, welcome to the board.

## 2023 Financials

Ron: Jeremy, can we get an informal report of the 2023 finances.

Jeremy: During 2023, we began transitioning income and expenses to the Redox OS nonprofit. This report reflects that transition.

| Item                        |    Amount |
| :-------------------------- | --------: |
| Income                      |           |
| Initial Donation            | 10,000.00 |
| Patreon (Feb-Dec)           |  6,747.79 |
| T-Shirts                    |    242.99 |
| Gross Income                | 16,990.78 |
|                             |           |
| Expenses                    |           |
| Community Manager (Oct-Dec) |  1,200.00 |
| Filings                     |     50.00 |
| Hosting (Oct-Dec)           |    345.60 |
| Gross Expenses              |  1,595.60 |
|                             |           |
| Net Income for 2023         | 15,395.18 |
|                             |           |
| Bank Balance                | 15,967.39 |
| Teespring/PayPal            |    242.99 |
| Credit Card                 |    815.20 |
| Net Assets at Dec. 31, 2023 | 15,395.18 |

Jeremy: A formal report will be presented at the next board meeting.

## 2024 Budget

Ron: I would like to present the budget for 2024.
These are the assumptions that were made:

- No new source of donations is included.
They will be added to the budget as the donations are received.
- The funding for the trademark application has been carried forward
from 2023, and increased slightly.
It is still only an estimate based on some preliminary research.
- There are two Redox Summer of Code projects included in this budget.
If we can get our projects directly funded,
for example by Google Summer of Code, we may not use that money.

|                        | Current | Monthly |  Annually  |
| :--------------------- | ------: | ------: | ---------: |
| Expenses	                                              |
| Server Hosting         |         |     120 |     1440   |
| Community Manager      |         |     400 |     4800   |
| Misc.                  |         |         |      500   |
| Legal and Trademark    |         |         |     5000   |
| Accounting and Filings |         |         |      500   |
| Redox Summer of Code   |         |         |    11000   |
| **Expenses Total**     |         |         |  **23240** |
|                                                         |
| Income		                                          |
| Patreon                |         |     600 |     7200   |
| T-Shirts               |         |      30 |      360   |
| Other Donations/Grants TBD			                  |
| **Income Total**       |         |         |   **7560** |
|                                                         |
| **Deficit**            |         |         | **-15680** |
|                                                         |
| Bank Balance           |  16,249 |         |            |
| Credit Card            |     915 |         |            |
| **Final Balance**      |  15,334 |         |   **-346** |


Ron: Can I get a motion to adopt the budget as presented, and authorize the treasurer to make expenditures according to the budget?

Alberto: Moved.

Jacob: Seconded.

Ron: Does anyone have any questions, changes or any further discussion regarding the budget?

Discussion: ...

Ron: If there is no further discussion, it is moved and seconded that the budget be adopted as presented, and the treasurer be authorized to make expenditures according to the budget. Those in favor say Aye, those opposed say No.

Jeremy: Aye.

Alberto: Aye.

Jacob: Aye.

Ron: Upon motion duly moved, seconded and carried, it was resolved that the budget be adopted as presented, and the treasurer is authorized to make expenditures according to the budget.

## Fundraising Plan

Ron: Could I get a motion to discuss the fundraising plan?

Jeremy: Moved.

Jacob: Seconded.

Ron: There is an MR pending on the nonprofit repo that provides a draft funding plan.
I would like to discuss some of the key points and the urgent items.

Ron: To date, we have been focusing on funding for RSoC.
We need to continue that, but some of the larger sovereign funds and foundations have minimum amounts that are quite large.
For example, the German Sovereign Tech Fund has a minimum proposal value of 150,000 Euro.

We need to agree on the "ask" for various types of donor organizations.
An "ask" is the amount of money we want and a list of concrete things the money will be used for.

We need to develop an ask for various different levels of funding.
I have been winging it so far, but I think as BDFL, Jeremy, you should be driving the ask.

We need to identify what summer projects we would like have done, and who can act as mentors.
I am available to mentor this summer, and I feel like there are some areas where I can provide adequate mentoring, but there are areas where I am not qualified.

Mentor applications for Google Summer of Code are now open.
I don't want to set expectations too high.
GSoC is mainly for people who are new to open source, as opposed to people who are current FOSS contributors.
We need to give a list of projects, and we should assume that these are people who are new to Redox.

For sovereign funds, I think we need to think big, but it will similarly be identifying a list of specific projects.
However, in those cases, we can assume we are trying to fund some of our key contributors, or finding new high-end contributors.

Other than approaching these foundations and other established sources of funds, I think we need to try to build partnerships with corporations.
I think we should be approaching vendors like NVidia, Intel, Arm, etc. and making sure they are aware of us.
Even if it is too early to push for large donations, we need to start the conversation with them and figure out how to make them comfortable with funding us.

There are a few other topics mentioned in the funding plan,
and I would like to get some feedback.
Alberto and I are happy to work on fundraising, but we need to get on the same page with you, Jeremy, so we are able to ask for the right things and present Redox in the right way.

Jeremy, would you like to add anything, or do you want to make a motion to postpone the discussion until the MR is reviewed?

Jeremy:

1. (Open discussion of funding and project priorities)
   
   or, when ready

2. I move that we postpone the discussion until we have reviewed the MR.

(If/when Jeremy moves that we postpone the discussion)

Jacob: Seconded.

Ron: If there is no further discussion...

Ron: It's moved and seconded that further discussion of the Funding Plan be postponed until the MR has been reviewed. Those in favor say Aye, those opposed say No.

Jeremy: Aye.

Alberto: Aye.

Jacob: Aye.

## Adding a Nonprofit-public Repo

Ron: I would like to make much of the nonprofit Gitlab repo public.
In particular, I would like that the following items be public.

- Bylaws
- Conflict of Interest Policy
- Minutes
- Budget
- Financial reports

Ron: Can I get a motion to request Jeremy to create a public repo for the nonprofit, and to have Alberto coordinate moving the above documents to the public repo?

Jeremy: Moved.

Jacob: Seconded.

Ron: Is there any discussion about creating a public nonprofit repo, and which documents should be made public?

Discussion: ...

Ron: If there is no further discussion, it is moved and seconded that a we request Jeremy to create public repo for the nonprofit, and that Alberto coordinate moving the previously mentioned documents to the public repo.
Those in favor say Aye, those opposed say No.

Jeremy: Aye.

Jacob: Aye.

Alberto: Aye.

Ron: Upon motion duly moved, seconded and carried, we request Jeremy to create a public repo for the nonprofit, and that Alberto should coordinate moving the mentioned documents to the public repo.

## Agenda for next meeting

I would like to propose the following agenda for the next board meeting, to take place no later than June 30.

- Approval of the minutes of this meeting
- Report on fundraising and promotion, and review of fundraising plan
- Report on trademark

Ron: Is there any other business to discuss at this time?

Ron: There being no other business, can I get a motion to adjourn?

Jeremy: Moved.

Jacob: Seconded.

Ron: It is moved and seconded that we adjourn. Those in favor say Aye, those opposed say No.

Alberto: Aye.

Jacob: Aye.

Jeremy: Aye.

Ron: The Ayes have it, the meeting is adjourned.