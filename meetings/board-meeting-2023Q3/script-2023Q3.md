
# Redox OS Board Meeting 2023Q3

## Call to order

I call the meeting of the Board of Directors of Redox OS to order.

## Attendance

Ron: When I state your name, please acknowledge your presence.

- Ronald Williams: Yes
- Jeremy Soller: Yes
- Alberto Souza: Yes

Ron: We have a quorum.

## Minutes from last meeting

Ron: The minutes for the previous board meeting have been provided to you. Are there any corrections?

Ron: If there are no corrections, ...

Ron: The minutes are approved as distributed.

## Document Filing

Ron: In the previous board meeting we took an action to ensure the Articles of Incorporation were filed in the corporate binder. The nonprofit will be using electronic books and records, as allowed under Colorado law. Since no corporate binder is required, we will just confirm that the Articles of Incorporation are filed with the state of Colorado.

Ron: Alberto, do you confirm that the Articles of Incorporation are filed with the Colorado Secretary of State.

Alberto: Yes.

## 501(c)(4) Status

Ron: Jeremy, can you confirm that the IRS has acknowledged receipt of our application for 501(c)(4) status? Can we declare ourselves a 501(c)(4) nonprofit corporation?

Jeremy: (confirm)

## Current Financial Status

Ron: Jeremy, can you please provide our current financial status?

Jeremy:

- Cash balance: $14,736.84
- The initial contribution of $10,000, which we called a loan at the last board meeting, is now considered a donation.
- Redox has no debt.

## Budget

Ron: I would like to propose a budget for the remainder of the calendar year.

- Monthly income:
  - Donations: Patreon $600
  - T-Shirts: We are hoping for $1,000-$2,000 to start, then $200 monthly after that
  - Total monthly revenue: $800
- Monthly expenses:
  - Hosting expenses: $100
  - Community coordinator: $400
  - Safety net (misc. expenses): $100
  - Total monthly expenses: $600
- Monthly Income Net: Approximately $200
- Trademark registration: $4,000

Ron: Are there any concerns regarding the budget?

Ron: I would like to entertain a motion to authorize the treasurer to make expenditures according to the stated budget.

Alberto: Moved.

Jeremy: Seconded.

Ron: It is moved and seconded that the treasurer is authorized to make expenditures according to the stated budget.

Ron: Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - No.

**RESOLVED**: Upon motion duly moved, seconded and carried, it was resolved that the treasurer is authorized to make expenditures according to the stated budget.

## Community Manager

Ron: Jeremy, can you talk about the community manager role.

Jeremy: It's our intent to employ Alberto as our community manager. He will continue doing the work he has been doing, helping people join GitLab and Matrix, answering questions, and coordinating with our other volunteers. We will be providing him with a monthly salary of $400, which we think is a very reasonable expense given the amount of time and the commitment he is putting into Redox and the community. It's important to note that this employment is not connected to his duties as a member of the board, which are voluntary.

Ron: I would like to entertain a motion that, subject to agreement on an employment contract, the board authorizes employing Alberto Souza in the position of community manager.

Jeremy: Moved.

Alberto: Seconded.

Ron: It is moved and seconded that, subject to agreement on an employment contract, the board authorizes employing Alberto Souza in the position of community manager. Those in favor say "Aye", those opposed say "No".

Jeremy and Ron: Aye.

**RESOLVED**: Upon motion duly moved, seconded and carried, it was resolved that, subject to agreement on an employment contract, the board authorizes employing Alberto Souza in the position of community manager.

## Fundraising

Ron: I will be spending more of my time over the next few months on fundraising. The first priority will be to find funds for more Summer of Code projects. There are a number of opportunities, including both direct and indirect funding. For example, for the Google Summer of Code, Google pays interns directly to work on open source projects.

We won't know how many Summer of Code projects we will have until we make more progress on funding. We had three RSoC projects this year.

As a second priority, I will be starting to look for corporate donors, which would hopefully get us to the point where we can hire one or two people full-time. But this is farther in the future.

As I speak to potential corporate donors, I will also be asking about in-kind contributions, where they contribute to the Redox code base. In some cases, it's easier for companies to do this rather than donate cash, because they don't have to worry about funds being misused.

## T-Shirts

Ron: Next is t-shirts. We have finalized our plans with t-shirts. We will be working with Teespring as our supplier because they give us the best balance between cost and simplicity. Teespring also has a lot of other products, which we might want to provide if people are happy with the t-shirts.

We will have premium t-shirts with just the Redox logo on the front. The logo has been updated, and we will be discussing that in a moment.

Ron: I would like to entertain a motion to authorize the treasurer to set up our t-shirt sales.

Alberto: Moved.

Jeremy: Seconded.

Ron: It is moved and seconded that the board authorize the treasurer to set up our t-shirt sales.

Ron: Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - No.

**RESOLVED**: Upon motion duly moved, seconded and carried, it was resolved that the board authorizes the treasurer to set up our t-shirt sales.

## Trademark plan

Ron: I would like to ask Jeremy for a report on our plans to trademark the Redox OS name.

Jeremy: We are planning to register the name Redox OS and our logo as international trademarks. We are changing our logo to incorporate the Redox OS name. The new logo is here:
https://gitlab.redox-os.org/redox-os/assets/-/blob/master/logos/redox/vectorized_logo.svg

Jeremy: We will be contacting trademark lawyers to get quotes for handling the application process.

Ron: Because Jeremy is the originator of Redox OS, we plan to have an agreement with him where the nonprofit takes responsibility for the Redox OS name and the nonprofit will officially recognize Jeremy as the BDFL. This agreement will be completed as we move forward with the trademark process.

Ron: I would like to entertain a motion that, subject to an agreement with Jeremy, and subject to the previously agreed budget, the board authorizes the treasurer to apply for trademarks of the Redox OS name and this logo on behalf of the nonprofit.

Jeremy: Moved.

Alberto: Seconded.

Ron: It is moved and seconded that, subject to an agreement with Jeremy, and subject to the previously agreed budget, the board authorizes the treasurer to apply for trademarks of the Redox OS name and this logo on behalf of the nonprofit.

Ron: Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - No.

**RESOLVED**: Upon motion duly moved, seconded and carried, subject to an agreement with Jeremy, and subject to the previously agreed budget, the board authorizes the treasurer to apply for trademarks of the Redox OS name and this logo on behalf of the nonprofit.

##  Development Priorities

Ron: I would like to ask Jeremy to report on Redox development.

Jeremy: (adapt this as you see fit)

We had a very successful summer of code, with significant contributions from 4lDO2, Andy and Enygmator, as well as from our volunteers.

We will be working on Release 0.9 of Redox in the next month or two. 

We have started to prioritize some of the work on Redox, which we hope will help give people some idea of what we are emphasizing. But as always, we want people to enjoy what they are doing, so feel free to work on what suits you.

- We need to have a stable ABI - work is needed in all areas.
- Redox as a Server room solution is slightly higher priority than desktop because it's a smaller scope.
- We want to self-host the build system - i.e. build Redox on Redox.
- We need to work on porting developer tools to Redox once the build system is ported.
- We would like to host the Redox website on Redox running in a virtual machine.
- There is lots of work to be done on virtual machine support, device virtualization, Redox as a hypervisor, etc.
- We need to start giving more attention to performance and responsiveness, and making Desktop and Demo go faster is an important part of that.
- Cosmic Desktop will be ported to Orbital as more pieces are completed, expect some visible progress in the first half of 2024.

## Agenda for Next Board Meeting

Ron: The following items are proposed for the next board meeting, to take place before December 31.

- Review of the minutes of this meeting
- Report on fundraising
- Report on t-shirt sales
- Report on trademark registration
- Discussion of upcoming board of directors elections

## Adjournment

Ron: Is there any other business for the board to consider at this time?

Discussion.

Jeremy: I move to adjourn.

Alberto: Seconded.

Ron: It is moved and seconded that we adjourn. Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - No.

This meeting is adjourned.