# Redox OS Board Meeting 2023Q3

The Redox OS Board Meeting for 2023Q3 took place on Sept. 27, 2023 at 3pm MDT (UTC-6).
A video of the meeting can be found on [YouTube](https://www.youtube.com/watch?v=LqBjQ-MjwqQ).

## Attendance

- Ronald Williams, president: present
- Jeremy Soller, vice president and treasurer: present
- Alberto Souza, secretary: present

## Minutes from last meeting

Resolved: The minutes from the Redox OS Board Meeting 2023Q2 were accepted. The minutes may be found in the [nonprofit-private repository on GitLab](https://gitlab.redox-os.org/redox-os/nonprofit-private/-/blob/main/board-meeting-2023Q2/minutes-2023Q2.md?ref_type=heads).

## Document Filing

The secretary confirms that the Articles of Incorporation are filed with the Colorado Secretary of State.

## 501(c)(4) Status

The treasurer confirms that the IRS has received our application for 501(c)(4) status and we can declare ourselves a 501(c)(4) nonprofit corporation.

## Current Financial Status

- Cash balance: $14,736.84
- The initial contribution of $10,000, which we called a loan at the last board meeting, is now considered a donation.
- Redox has no debt.

## Budget

Resolved: The budget for the remainder of the 2023 calendar year was accepted.

- Monthly income:
  - Donations: Patreon $600
  - T-Shirts: We are hoping for $1,000-$2,000 to start, then $200 monthly after that
  - Total monthly revenue: $800
- Monthly expenses:
  - Hosting expenses: $100
  - Community coordinator: $400
  - Safety net (misc. expenses): $100
  - Total monthly expenses: $600
- Monthly Income Net: Approximately $200
- Trademark registration: $4,000

Resolved: The treasurer is authorized to make expenditures according to the stated budget.

## Community Manager

Resolved: Subject to agreement on an employment contract, the board authorizes employing Alberto Souza in the position of community manager.

## Fundraising

The president provided a brief report on plans for fundraising, with a focus on funding for Redox Summer of Code.

## T-Shirts

Resolved: The board authorizes the treasurer to set up t-shirt sales.

## Trademark plan

Resolved: Subject to an agreement with Jeremy regarding ownership of the Redox OS name and the BDFL title, and subject to the previously agreed budget, the board authorizes the treasurer to apply for trademarks of the Redox OS name and the Redox OS logo on behalf of the nonprofit.

##  Development Priorities

Jeremy provided a report on Redox OS development priorities. A document describing those priotiries is available as a [news post](https://redox-os.org/news/development-priorities-2023-09) on the Redox website.

## Agenda for Next Board Meeting

The following items are proposed for the next board meeting, to take place before December 31.

- Review of the minutes of this meeting
- Report on fundraising
- Report on t-shirt sales
- Report on trademark registration
- Discussion of upcoming board of directors elections

## Adjournment

The meeting was adjourned at 3:25pm MDT.
