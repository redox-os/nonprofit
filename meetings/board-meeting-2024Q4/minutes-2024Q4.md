# Redox OS Board Meeting 2024Q4

The Redox OS Board Meeting for 2024Q4 took place on December 18, 2024 at 12:00pm MDT (UTC-7).
A video of the meeting is posted on [the Redox OS YouTube Channel](https://www.youtube.com/watch?v=810IkVaAejg).

## Attendance

- Ron Williams, president: present
- Jeremy Soller, vice president and treasurer: present
- Alberto Souza, secretary: present
- Jacob Schneider, director: present

## Minutes from last meeting

Resolved: The minutes from the Redox OS Board Meeting 2024Q3 were accepted. The minutes are available in the
[nonproft repository on GitLab](https://gitlab.redox-os.org/redox-os/nonprofit/-/blob/main/meetings/board-meeting-2024Q3/minutes-2024Q3.md?ref_type=heads).

## Finances

Jeremy reported on the Nonprofit finances.

| Account      |       Sep 25 |    Dec 16 |
| :----------- | -----------: | --------: |
| Bank Account |   26,105.06  | 22,807.07 |
| Credit Card  |  (1,782.95)  |  (954.38) |
| Teespring    |      409.91  |     95.82 |
| Bitcoin      |       63.63  |    138.33 |
| Ethereum     |      430.70  |    651.01 |
| Radicle/USDC |    4,134.69  |  4,684.30 |
| Radworks     |   12,459.08* |           |
| **Total**    |   41,820.12  | 27,422.15 |
| Total (no Radworks) | 29,361.04 | 27,422.15 |

## Redox Governance

The Redox Governance Document was accepted. The Governance Document is available in the [nonprofit repository](https://gitlab.redox-os.org/redox-os/nonprofit/-/blob/main/Governance.md).

## Development Budget

It was proposed that we allocate approximately $15,000 from our general budget
towards paid projects, including a southern hemisphere Summer of Code project.

A list of potential projects was included in the monthly report for November.
Unfortunately, no one other than our Summer of Code contributor
has shown interest in getting funding.

Jacob has some contacts and suggestions,
particularly for the universities.

We have a couple of capstone projects, one for a system health monitor
and one for graphics and VirtIO-GPU.

Ron will work on a development plan with ideas on how to use
our funds to attract more contributors.

## Fundraising

There has been a shift in Europe away from public funding of open source projects.
NLnet is an exception to this, they have a new fund running until 2027,
with up to 500,000 Euros total for an open source project.
We have submitted two proposals to NLnet.
We will need a second developer if both projects are approved.
Ron plans to submit an additional proposal for 50,000 Euros.

We will need to look for other sources of grants and donations. We will discuss it in the new year.

## Other Items

- Coordinating Contributors - Action: Ron to try to use GitLab Projects.
- Website - Action: Jacob to create an MR and Ron to review.
- Trademark - Action: Jeremy to draft a Redox trademark document based on COSMIC's.
- RustConf Trip Report - Ron presented to a group of about 30 people, in a quick demo of Redox.
Most of the discussions Ron had at RustConf from people interested in Redox were about containers, jails, sandboxing and trust. This action item is closed.

## Agenda for next meeting

- Approval of the minutes of this meeting
- Election of Directors for 2025
- Summary of 2024 finances
- Budget for 2025
- Development plan

## Adjournment

The meeting was adjourned at 12:35pm MDT.

