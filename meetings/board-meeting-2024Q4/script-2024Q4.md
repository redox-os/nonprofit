# Board Meeting Script 2024Q3

## Preamble

Ron: Welcome to the Redox OS board meeting for Q4 of 2024.

## Call to order

Ron: I call the meeting of the Board of Directors of Redox OS to order.

## Attendance

Ron: When I state your name please acknowledge your presence.

- Jeremy Soller: Present
- Alberto Souza: Present
- Jacob Schneider: Present
- Ron Williams: Present

Ron: We have a quorum.

## Minutes from last meeting

Ron: The minutes from the previous board meeting are available on GitLab, and have been provided to you for review.
Are there any corrections?

Ron: If there are no corrections...

Ron: The minutes are approved as distributed.

## Finances

Ron: Can I get a motion to have the treasurer give an update on our finances?

Alberto: Moved.

Jacob: Seconded.

Ron: I previously included some additional money from Radworks in the total,
but I believe part of it has been double counted, so I think we should ignore any
remaining amount at Radworks until we are able to withdraw it.

Ron: Jeremy, could you please provide an update on finances?

Jeremy: Our account balances at the end of last quarter and the end of last month are as follows.

| Account      |       Sep 25 |    Dec 16 |
| :----------- | -----------: | --------: |
| Bank Account |   26,105.06  | 22,807.07 |
| Credit Card  |  (1,782.95)  |  (954.38) |
| Teespring    |      409.91  |     95.82 |
| Bitcoin      |       63.63  |    138.33 |
| Ethereum     |      430.70  |    651.01 |
| Radicle/USDC |    4,134.69  |  4,684.30 |
| Radworks     |   12,459.08* |           |
| **Total**    |   41,820.12  | 27,422.15 |
| Total (no Radworks) | 29,361.04 | 27,422.15 |

## Redox Governance

Ron: Can I get a motion to discuss the Redox Governance Document?

Jacob: Moved.

Alberto: Seconded.

Ron: A Governance document for the Redox project and the nonprofit
has been published in the Nonprofit repo,
(https://gitlab.redox-os.org/redox-os/nonprofit/-/blob/main/Governance.md)
and has been reviewed by the board.
This document is required for our application to Kraken
for us to obtain a corporate account, among other things.
Can I get a motion to adopt the Governance document?

Jacob: Moved.

Alberto: Seconded.

Ron: It has been moved and seconded that we adopt the Governance document as presented.
Those in favor say Aye, those opposed say no.

Alberto, Jacob, Jeremy: Aye (or no)

Ron: The Governance document is adopted.

## Development Budget

Ron: Can I get a motion to discuss the development budget?

Jeremy: Moved.

Jacob: Seconded.

Ron: It was proposed that we allocate approximately $15,000 from our general budget
towards paid projects, including a southern hemisphere Summer of Code project.
We had allocated $11,000 at the start of the year towards Summer of Code,
and we have spent none of it so far,
so this is only a $4,000 increase in development funding.

A list of potential projects was included in the monthly report for November.
Unfortunately, no one other than our Summer of Code contributor
has shown interest in getting funding.

I will work on a new development plan with some ideas about how to
use our available funds to attract more contributors.

Ron: Are there any questions or concerns?

## Fundraising

Ron: Can I get a motion to discuss fundraising?

Jeremy: Moved.

Jacob: Seconded.

Ron: There has been a big shift in Europe away from public funding of open source projects.
The one exception is NLnet. NLnet started a new fund in 2024, running until 2027.
They have a total of 21M EUR, with up to 500,000 EUR for a single open source project.

I have submitted two proposals to NLnet for work beginning next summer.
These projects both identified 4lDO2 as the developer,
with the caveat that if both projects are approved,
we will need an additional developer.
I don't know who that developer would be at this point.

I plan to submit a larger proposal to NLnet for the February 1 call.
We need to discuss what the project should cover,
but that can wait until January.

We will need to take another look at possible sources of grants and donations.
I have some ideas that we can discuss after the holidays.

Are there any questions or concerns?

## Other items

Ron: Can I get a motion to discuss the other action items from our last meeting?

Jeremy: Moved.

Alberto: Seconded.

Ron: The following action items were identified at our last meeting.

- Coordinating Contributors - Jeremy and I took an action to look into how to better
coordinate work on larger items with more contributors.
We discussed using GitLab Projects.
I intend to try setting up some projects in GitLab to see if it helps. Action: Ron

- Website - Jacob and I have started discussing the updates to the website.
Jacob will create a draft MR and he and I will work through comments and suggestions
on the MR. Action: Jacob and Ron

- Trademark - I don't believe there's anything new regarding the trademark.
The action is for Jeremy to modify the trademark document from COSMIC.

- RustConf Trip Report - I won't be completing a written trip report.
The presentations from the main rooms have been posted on the Rust Foundation's
YouTube channel.
I was able to present Redox to about 30 people in a short, improvised presentation.
I was able to speak to ARM about Redox, but nothing has come of that yet.

The key point that I want to raise from the conference was that almost every question
I was asked related to containers, jails, sandboxing and trust.

I would like to consider this action item closed.

Are there any questions or concerns on any of these items?

## Agenda for next meeting

Ron: I would like to propose the following agenda for the next board meeting, to take place no later than January 31.

- Approval of the minutes of this meeting
- Election of Directors for 2025
- Summary of 2024 finances
- Budget for 2025
- Development Plan
- Report on the above items

Ron: Is there any other business to discuss at this time?

Ron: There being no other business, can I get a motion to adjourn?

Jeremy: Moved.

Jacob: Seconded.

Ron: It is moved and seconded that we adjourn. Those in favor say Aye, those opposed say No.

Alberto: Aye.

Jacob: Aye.

Jeremy: Aye.

Ron: The Ayes have it, the meeting is adjourned.