# Redox OS First Board Meeting

## Preamble

Welcome. This meeting will establish the Board of Directors and the Bylaws for the nonprofit.

## Call to order

I call the meeting of the Board of Directors of Redox OS to order.

## Attendance

Ron: When I state your name please acknowledge your presence.

- Ronald Williams (Ron): Yes
- Jeremy Soller (Jeremy): Yes
- Alberto Souza (Alberto): Yes

Ron: We have a quorum.

## Appoint a Temporary Chair and Secretary

Jeremy: I move that Ronald Williams be appointed as temporary chair for this meeting.

Alberto: Seconded.

Ron: It is moved and seconded that I, Ronald Williams, be appointed as temporary chair for this meeting. Is there any discussion?

Ron: Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - No.

**RESOLVED**: Upon motion duly moved, seconded and carried, it was resolved that Ronald Williams would serve as temporary chair of the meeting.

Jeremy: I move that Alberto Souza be appointed as temporary secretary for this meeting.

Ron: Seconded.

Ron: It is moved and seconded that Alberto Souza be appointed as temporary secretary for this meeting. Is there any discussion?

Ron: Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - No.

**RESOLVED**: Upon motion duly moved, seconded and carried, it was resolved that Alberto Souza would serve as temporary Secretary of the meeting.

## Report on Filing of Articles of Incorporation

Ron: I would like to request a report from Jeremy on the Articles of Incorporation.

Jeremy: Report.

## Filing of Articles of Incorporation

Ron: I move that we direct the secretary of this corporation to ensure that a copy of the signed and certified articles of incorporation is placed in a corporate binder to be kept at the principal office.

Jeremy: Seconded.

Ron: It is moved and seconded that we direct the secretary of this corporation to ensure that a copy of the signed and certified articles of incorporation is placed in a corporate binder to be kept at the principal office. Is there any discussion?

Ron: Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - No.

**RESOLVED**: Upon motion duly moved, seconded and carried it was resolved that the secretary of the corporation be directed to ensure a copy of the signed and certified articles of incorporation be placed in a corporate binder to be kept at the principal office.

## Adoption of Bylaws

Ron: I would like to present the Redox OS Bylaws, which can be found at the link below.

https://gitlab.redox-os.org/redox-os/nonprofit/-/blob/main/Bylaws.odt

If you could, please download and open the Bylaws. I would like to confirm that each of you has retrieved and reviewed the Redox OS Bylaws, from the link presented here.

Jeremy: Yes.

Alberto: Yes.

Ron: As have I. I move that the presented bylaws be adopted as the bylaws of this organization.

Jermy: Seconded.

Ron: It is moved and seconded that he presented bylaws be adopted as the bylaws of this organization. Is there any discussion?

Ron: Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - No.

**RESOLVED**: Upon motion duly moved, seconded and carried it was resolved that the presented bylaws be adopted as the bylaws of this organization.

## Adoption of Conflict of Interest Policy

Ron: I would like to present the Redox OS Conflict of Interest Policy and Agreement, which can be found at the link below.

https://gitlab.redox-os.org/redox-os/nonprofit/-/blob/main/Conflict_of_Interest_Policy.odt

If you could, please download and open the Conflict of Interest Policy. I would like to confirm that each of you has retrieved and reviewed the Redox OS Conflict of Interest Policy and Agreement, from the link presented here.

Jeremy: Yes.

Alberto: Yes.

Ron: As have I. I move that the presented policy be adopted as the conflict of interest policy of this organization.

Jeremy: Seconded.

Ron: It is moved and seconded that the presented policy be adopted as the conflict of interest policy of this organization. Is there any discussion?

Ron: Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - No.

**RESOLVED**: Upon motion duly moved, seconded and carried it was resolved that the presented conflict of interest policy be adopted as the conflict of interest policy of this organization.

## Appointment of Officers

Jeremy: I nominate Ronald Williams to the office of board president and director.

Alberto: Seconded.

Ron: It is moved and seconded that I, Ronald Williams, will serve as board president and director. Is there any discussion?

Ron: Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - No.

**RESOLVED**: Upon motion duly moved, seconded and carried, it was resolved that Ronald Williams would serve as board president and director.

Ron: I nominate Jeremy Soller to the office of board vice-president and director.

Alberto: Seconded.

Ron It is moved and seconded that Jeremy Soller serve as board vice-president and director. Is there any discussion.

Ron: Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - No. Carried.

**RESOLVED**: Upon motion duly moved, seconded and carried, it was resolved that Jeremy Soller would serve as board vice-president and director.

Ron: I nominate Alberto Souza to the office of secretary and director.

Jeremy: Seconded.

Ron: It is moved and seconded that Alberto Souza serve as secretary and director. Is there any discussion?

Ron: Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - No.

**RESOLVED**: Upon motion duly moved, seconded and carried, it was resolved that Alberto Souza would serve as secretary and director.

Ron: I nominate Jeremy Soller to the office of treasurer.

Alberto: Seconded.

Ron: It is moved and seconded that Jeremy Soller serve as treasurer. Is there any discussion?

Ron: Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - No.

**RESOLVED**: Upon motion duly moved, seconded and carried, it was resolved that Jeremy Soller would serve as treasurer.

## Designate Principal Office

Ron: I would like to present to you the proposed location of the principal offices of this organization. You should have received an email from Jeremy titled "Principal Offices". Please open that email.

Ron: I move that the principal office of this organization be located at the address presented.

Alberto: Seconded.

Ron: It is moved and seconded that the principal office be located at the address presented. Is there any discussion?

Ron: Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - No. Carried.

**RESOLVED**: Upon motion duly moved, seconded and carried, it was resolved that the principal office of this organization is as presented.

## Report on Finances

Ron: I would like to request that Jeremy present the state of the organization's finances.

Jeremy: Report.

## Corporate Bank Account

Ron: I move to authorize the use of the corporate bank account described by Jeremy.

Alberto: Seconded.

Ron: It is moved and seconded that use of the corporate bank account Jeremy described be authorized for the organization. Is there any discussion?

Ron: Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - No. Carried.

**RESOLVED**: Upon motion duly moved, seconded and carried, it was resolved that the use of the corporate bank account is authorized for the organization.

## Expenditures

Ron: I move to authorize the treasurer to act as signatory for all corporate bank accounts.

Alberto: Seconded.

Ron: It is moved and seconded that the treasurer be the signatory for all corporate bank accounts. Is there any discussion?

Ron: Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - Nay. Carried.

**RESOLVED**: Upon motion duly moved, seconded and carried, it was resolved that the treasurer is authorized as signatory for all corporate bank accounts.

Ron: I move that until such time as a budget is approved by the board, any expenditures must be approved by a minimum of two board members.

Jeremy: Seconded.

Ron: It is moved and seconded that the until a budget is approved by the board, any expenditures must be approved by a minimum of two board members. Is there any discussion?

Ron: Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - Nay. Carried.

**RESOLVED**: Upon motion duly moved, seconded and carried, it was resolved that until a budget is approved by the board, any expenditures must be approved by a minimum of two board members.

## File for Tax Exemption

Ron: I move to authorize the treasurer's application for tax-exempt status as a nonprofit social welfare organization under Section 501(c)(4) of the Internal Revenue Code.

Jeremy: Seconded.

Ron: It is moved and seconded that the treasurer's application for tax-exempt status as a nonprofit social welfare organization under Section 501(c)(4) of the Internal Revenue Code be authorized. Is there any discussion?

Ron: Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - No. Carried.

**RESOLVED**: Upon motion duly moved, seconded and carried, it was resolved that the treasurer's application for tax-exempt status as a nonprofit social welfare organization under Section 501(c)(4) of the Internal Revenue Code be authorized.

## Agenda for Next Board Meeting

Ron: The following items are proposed for the next board meeting, to take place within the next three months.
- Review of minutes of this meeting
- 2023 Budget
- Plan for management of the Redox OS trademarks, websites and technical content

## Adjournment

Ron: Is there any other business for the board to consider at this time?

Discussion.

Ron: I move to adjourn.

Jeremy: Seconded.

Ron: It is moved and seconded that we adjourn. Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - No. Carried.

This meeting is adjourned.