# Preparation for Director's Meeting

## Ron

- [x] Prepare detailed agenda for meeting
- [x] ToDo list for next meeting (August)
- [ ] Pick a date for the next meeting (August)
- [x] Final review of Bylaws

## Jeremy

- [ ] Report on Articles of Incorporation (AoI)
  - [ ] Date of Incorporation
    - 2023-02-13
  - [ ] Who signed, who has been appointed?
    - Jeremy Soller
  - [ ] What is the stated purpose (if any)?
    - A stated purpose is not required in the articles of incorporation for a 501(c)(4) organization, but is provided in the bylaws.
  - [ ] As far as you know, is there any issue WRT AoI meeting 501(c)(4)?
    - There are no known issues with meeting 501(c)(4) with the current articles of incorporation
  - [ ] What is the process for adding the board to the AoI?
    - I do not believe it is required to state the current composition of the board in the articles of incorporation for a 501(c)(4) organization. The bylaws provide legal rights to the board of directors

- [ ] Current state of finances of nonprofit
  - [ ] Assets - what bank, current balance
    - The Redox OS Chase Bank checking account holds 12,867.67 USD as of 2023-06-21
  - [ ] Debts if any
    - No debts
  - [ ] Monthly income forecast (as is)
    - Aproximately 750 USD monthly, from Patreon
  - [ ] Planned and committed expenses (as is)
    - Redox OS GitLab cost (about 100 USD monthly) should be transitioned to the Redox OS checking account
  - [ ] Logistics - checks, credit cards, Patreon donations (as is)
    - Patreon donations are the primary source of income
    
- [ ] Plans for books and records (preliminary)
  - [ ] Paper records
    - Paper records should be done as desired. Digital copies (with backups at the primary office, Jeremy Soller's home) are sufficient
  - [ ] On-line records
    - When all board members approve, then this repository can be made public and most documents placed here
  - [ ] Signing, shipping, storing
    - Signing via printing and scanning is recommended. The final scan with all signatures should be stored here
  - [ ] Government filings
    - A yearly financial report must be provided to the IRS. I will take care of this, and can provide copies in this repository
  - [ ] How Ribbon can confirm paper documents in Jeremy's possession are in order
    - I believe we do not need to have much paper documentation, so long as documents are stored digitally in this repository and that is cloned accross a number of machines. With GPG signatures, it provides good verification that the right data is replicated
