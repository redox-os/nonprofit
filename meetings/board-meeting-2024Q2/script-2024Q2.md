# Board Meeting Script 2024Q2

## Preamble

Ron: Welcome to the Redox OS board meeting for Q2 of 2024.

## Call to order

Ron: I call the meeting of the Board of Directors of Redox OS to order.

## Attendance

Ron: When I state your name please acknowledge your presence.

- Jeremy Soller: Present
- Alberto Souza: Present
- Jacob Schneider: Present
- Ron Williams: Present

Ron: We have a quorum.

## Minutes from last meeting

Ron: The [minutes from the previous board meeting](https://gitlab.redox-os.org/redox-os/nonprofit/-/blob/main/meetings/board-meeting-2024Q1/minutes-2024Q1.md?ref_type=heads) are available on GitLab, and have been provided to you for review.
Are there any corrections?

Ron: If there are no corrections...

Ron: The minutes are approved as distributed.

## Decisions since last meeting

Ron: The following items were decided by vote of the board between the last meeting and now.

It was resolved that the nonprofit will accept donations via Bitcoin and Ethereum wallets, and Jeremy was authorized to create the wallets.

It was resolved that Alberto's monthly stipend is increased from $400 to $800 to recognize the scope of his work, and Jeremy is authorized to pay it.

It was resolved that $472 advance will be provided to Alberto, to be subtracted from the December stipend, and Jeremy is authorized to pay it.

Ron: Are there any concerns with this statement of resolutions?

Ron: There being no concerns, this statement of resolutions is approved.

## Finances

Ron: Can I get a motion to have the treasurer give an update on our finances?

Alberto: Moved.

Jacob: Seconded.

Ron: Jeremy, could you please provide an update on finances?

Jeremy: Our account balances at the end of last quarter and the end of last month are as follows.

| Account      |   March 31 |     May 31 |
| :----------- | ---------: | ---------: |
| Bank Account |  25,920.77 |  28,975.35 |
| Credit Card  |   (915.20) | (2,239.63) |
| Teespring    |     130.25 |     172.08 |
| Bitcoin      |          - |      67.50 |
| Ethereum     |          - |      84.50 |
| **Total**    |  25,135.82 |  27,059.80 |

- We have received funding from Radworks of $12,566.30.
- Patreon and Merch sales is slightly below the budget amount and is approximately $500 per month.
- Jeremy has donated $3,200 to cover the increased stipend for Alberto.
- Our typical monthly spending is now at $915.20.
- We have only identified one Redox Summer of Code project, although we had budgeted for two. 

Jeremy: Applying these changes to our annual budget, we now estimate
- $20,940 in expenses for the year.
- $21,526 in income.
- A net gain of $586
- We expect to end the year with approximately $15,920 in cash.

Jeremy: Our tax filing this year was a 990-N, which is a simple form stating that we had less than $50,000 in revenue in 2023.
We are not required to provide a complete financial report.

Jeremy: As many large donors consider a nonprofit's financial health as a factor in making donations, it's in our interest to make a more complete financial report available.

Jeremy: For 2023, the report was written by Ron and has not been audited.
The report is available on GitLab.

Jeremy: I move that the 2023 Financial Report be adopted.

Jacob: Seconded.

Ron: It is moved and seconded that the 2023 Financial Report be adopted.
Does anyone have any questions, changes or further discussion about the financial report?

Discussion: ...

Ron: If there is no further discussion, it is moved and seconded that the 2023 Financial Report be adopted as presented. Those in favor say Aye, those opposed say No.

Jeremy: Aye.

Alberto: Aye.

Jacob: Aye.

Ron: Upon motion duly moved, seconded and carried, it was resolved that the 2023 Financial Report be adopted as presented.

## Budget Update

This table is for background only, and does not need to be presented.

|                            | Jan. 1, 2024 |      Monthly |     Annually |
| :------------------------- | -----------: | -----------: | -----------: |
| Expenses	                 |              |              |              |
| Server Hosting             |              |          120 |        1,440 |
| Community Manager          |              |          800 |        8,000 |
| Misc.                      |              |              |          500 |
| Legal and Trademark        |              |              |        5,000 |
| Accounting and Filings     |              |              |          500 |
| Redox Summer of Code       |              |              |        5,500 |
| **Expenses Total**         |              |              |   **20,940** |
|                            |              |              |              |
| Income                     |              |              |              |
| Jeremy                     |              |              |        3,200 |
| Radworks                   |              |              |       12,566 |
| Patreon                    |              |          450 |        5,400 |
| Merch                      |              |           30 |          360 |
| Other Donations/Grants TBD |              |              |              |
| **Income Total**           |              |              |   **21,526** |
|                            |              |              |              |
| **Gain/Loss**              |              |              |      **586** |
|                            |              |              |              |
| Bank Balance               |       16,249 |              |              |
| Credit Card                |        (915) |              |              |
| **Net Cash**               |       15,334 |              |   **15,920** |

## Fundraising

Ron: Can I get a motion to discuss fundraising?

Jeremy: Moved.

Jacob: Seconded.

Ron: Thank you.

Our main focus for fundraising recently has been on a few government-funded open source institutions.
The most relevant of these institutions are based in the EU, and some of them have a preference for EU-based work.
We have also applied to a US-based institution.

The response to our project proposals has generally been positive.
I'm very optimistic about our ability to get funding from some of the European institutions.
They seem to genuinely appreciate Redox and what we are trying to do.
There seems to be a 4-6 month lead time for getting proposals accepted and underway.

We have also just started contacting hardware vendors.

Our Patreon funding fallen slightly in the last 12 months, and our efforts at promotion so far have not produced much results there.
I'm hoping that having Release 0.9 of Redox, and doing what we can to promote it, will bring more excitement and hopefully more small donors.

With regard to our merchandise sales, we are still selling a few t-shirts per month. There seems to be a small correlation between our promotion efforts and our merchandise sales, but we will keep an eye on that.

We are working on sweatshirts and coffee mugs, but they are not ready to go yet.

With regards to promotion, we are posting monthly newsletters, and they are getting picked up by Phoronix, as well as being promoted by Jeremy on various social media accounts.
The correlation between Jeremy's promotion and Teespring views seems to be higher than Phoronix to Teespring views, but the timing is close so it's hard to be sure.

We will be writing proposals for NGI Sargasso and the German Sovereign Tech Fund in the next few weeks. I have made some suggestions for project content, and we can discuss those further over the next few days.

Ron: We are also starting to contact more hardware vendors, although there is nothing to report on that front.

Ron: Does anyone have any concerns regarding fundraising?

Ron: As there are no concerns, we will move on.

## Trademark

Ron: Can I get a motion to discuss the trademark?

Jacob: Moved.

Alberto: Seconded.

Jeremy: COSMIC is using an unregistered trademark, and the COSMIC team has adopted a policy for it. The link for the policy is here.
https://github.com/pop-os/cosmic-epoch/blob/master/TRADEMARK.md

Jeremy: I propose that we not pursue trademark registration at this time, and that we create a policy for the use of the Redox OS name.

Ron: So moved. Can I get a second?

Jacob: Seconded.

Ron: It is moved and seconded that we not pursue trademark registration at this time, and that we create a policy for the use of the Redox OS name as an unregistered trademark. Does anyone have any questions, changes or further discussion about the trademark policy?

Discussion: ...

Ron: If there is no further discussion, it is moved and seconded that we not pursue trademark registration at this time, and that we create a policy for the use of the Redox OS name as an unregistered trademark. Those in favor say Aye, those opposed say No.

Jeremy: Aye.

Alberto: Aye.

Jacob: Aye.

Ron: Upon motion duly moved, seconded and carried, it was resolved that we not pursue trademark registration at this time, and that we create a policy for the use of the Redox OS name as an unregistered trademark.

## Website

Ron: Can I get a motion to discuss the website?

Jeremy: Moved.

Alberto: Seconded.

Jacob: *Jacob's report*

Ron: Are there any questions or concerns regarding Jacob's report?

Ron: There being no further discussion, I would like to move on.

## Agenda for next meeting

Ron: I would like to propose the following agenda for the next board meeting, to take place no later than September 30.

- Approval of the minutes of this meeting
- Report on year to date finances
- Report on fundraising activity
- Report on trademark
- Report on website

Ron: Is there any other business to discuss at this time?

Ron: There being no other business, can I get a motion to adjourn?

Jeremy: Moved.

Jacob: Seconded.

Ron: It is moved and seconded that we adjourn. Those in favor say Aye, those opposed say No.

Alberto: Aye.

Jacob: Aye.

Jeremy: Aye.

Ron: The Ayes have it, the meeting is adjourned.