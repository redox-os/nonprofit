# Redox OS Board Meeting 2024Q2

The Redox OS Board Meeting for 2024Q2 took place on June 6, 2024 at 11:00am MDT (UTC-6).
A video of the meeting is posted on [the Redox OS YouTube Channel](https://www.youtube.com/watch?v=zAKBJlpyje0)l

## Attendance

- Ron Williams, president: present
- Jeremy Soller, vice president and treasurer: present
- Alberto Souza, secretary: present
- Jacob Schneider, director-nominee: present

## Minutes from last meeting

Resolved: The minutes from the Redox OS Board Meeting 2024Q1 were accepted. The minutes are available in the [nonproft repository on GitLab](https://gitlab.redox-os.org/redox-os/nonprofit/-/blob/main/meetings/board-meeting-2024Q1/minutes-2024Q1.md?ref_type=heads).


## Decisions since the last meeting

The following items were decided by vote of the board between the last meeting and now.
Resolved: The nonprofit will accept donations via Bitcoin and Ethereum wallets, and Jeremy was authorized to create the wallets.
Resolved: Alberto's monthly stipend is increased from $400 to $800 to recognize the scope of his work, and Jeremy is authorized to pay it.
Resolved: A $472 advance will be provided to Alberto, for a new computer, to be subtracted from the December stipend, and Jeremy is authorized to pay it.

## Finances

| Account      |   March 31 |     May 31 |
| :----------- | ---------: | ---------: |
| Bank Account |  25,920.77 |  28,975.35 |
| Credit Card  |   (915.20) | (2,239.63) |
| Teespring    |     130.25 |     172.08 |
| Bitcoin      |          - |      67.50 |
| Ethereum     |          - |      84.50 |
| **Total**    |  25,135.82 |  27,059.80 |

- We have received funding from Radworks of $12,566.30.
- Patreon and Merch sales is slightly below the budget amount and is approximately $500 per month.
- Jeremy has donated $3,200 to cover the increased stipend for Alberto.
- Our typical monthly spending is now at $915.20.
- We have only identified one Redox Summer of Code project, although we had budgeted for two. 

Applying these changes to our annual budget, we now estimate
- $20,940 in expenses for the year.
- $21,526 in income.
- A net gain of $586
- We expect to end the year with approximately $15,920 in cash.

Our tax filing this year was a 990-N, which is a simple form stating that we had less than $50,000 in revenue in 2023.
We are not required to provide a complete financial report.

As many large donors consider a nonprofit's financial health as a factor in making donations, it's in our interest to make a more complete financial report available.

For 2023, the report was written by Ron and has not been audited.
The report is available [on GitLab](https://gitlab.redox-os.org/redox-os/nonprofit/-/blob/main/finances/Financial%20Report%202023.md?ref_type=heads).

Resolved: The 2023 Financial Report was adopted.

## Fundraising

Ron provided a report on fundraising.

Our main focus for fundraising recently has been on a few government-funded open source institutions.
The most relevant of these institutions are based in the EU, and some of them have a preference for EU-based work.
We have also applied to a US-based institution.
The response to our project proposals has generally been positive.

I'm very optimistic about our ability to get funding from some of the European institutions.
They seem to genuinely appreciate Redox and what we are trying to do.
There seems to be a 4-6 month lead time for getting proposals accepted and underway.

We have also just started contacting hardware vendors.
Our Patreon funding fallen slightly in the last 12 months, and our efforts at promotion so far have not produced much results there.
I'm hoping that having Release 0.9 of Redox, and doing what we can to promote it, will bring more excitement and hopefully more small donors.

With regard to our merchandise sales, we are still selling a few t-shirts per month. There seems to be a small correlation between our promotion efforts and our merchandise sales, but we will keep an eye on that.
We are working on sweatshirts and coffee mugs, but they are not ready to go yet.
I just got a first cut of the coffee mug and I'll be asking the communnity for their thoughts on whether we should go with this or make some changes. I'm hoping to see the sweatshirts next week

With regards to promotion, we are posting monthly newsletters, and they are getting picked up by Phoronix, which is a linux online blog journal and it's very popular amongst the Linux communnity.
They are being picked up very closely after we post them, so that's very positive. 
As well as being promoted by Jeremy on various social media accounts.

The correlation between Jeremy's promotion and Teespring views seems to be higher than Phoronix to Teespring views, but the timing is close so it's hard to be sure.
There doesn't seem to be a correlation between the views of the merchandise and the actual sales.

We will be writing proposals for NGI Sargasso, which is a fund for projects between Europe and North America, and the German Sovereign Tech Fund, which is general open source funding, in the next few weeks.
I have made some suggestions for project content, and we can discuss those further over the next few days.

We are also starting to contact more hardware vendors, although there is nothing to report on that front.
Jacob has a contact with a cloud provider, we are open to other suggestions about other coporations that could potentially fund redox in some fashion. 

## Trademark

The COSMIC desktop is using an unregistered trademark, and the COSMIC team has adopted a policy for it. The link for the policy is here.
https://github.com/pop-os/cosmic-epoch/blob/master/TRADEMARK.md

For COSMIC, the trademark has to cover COSMIC by itself, which is a very difficult thing to trademark, because of its' broad use.
However, so long as you have a specific ecosystem that you are applying a name to, and you are applying it as an adjective, for example the COSMIC desktop environment, COSMIC editor, COSMIC terminal, 
the unregistered trademark law provides almost equal protection to the registered trademark law.
The difference would be that the registered trademarks can prevent other companies from registering the same trademark in the same space,
and they usually give preferential treatment in any kind of dispute.
However the price for registering trademarks is significant and the likelihood of getting a trademark is low.
We would be applying for Redox OS as the trademark, and that would probably be an easier sell than Redox by itself, or the name COSMIC by itself.

Because the protection that an unregistered trademark already gets is something that I consider to be enough and COSMIC considered to be enough we can continue down that road having an unregistered trademark and later on,
if we decide to and if we have more budget for it, we can try to register the Redox OS name as a trademark.

We can use our trademark policy such as the one that is linked and get protection and make sure that other organizations are not infriging on our rights while planning on going through the registration process, at some indeterminate time in the future.

If we are using the trademark and selling things with the trademark then it would be very unlikely that a company would be able to register it, 
because we would be able to say, we are already marketing things using this name.

An unregistered trademark is actually a common law trademark.
Registered trademarks are governed by international agreements, but the common law is slightly different in every country.
In the US, it's different by state, but the general rule in the US is that if it's an active community then the trademark is treated as valid.

Gnome uses an unregistered trademark as well and again the difficulty is that name is one that has general usage.
Redox itself is something we could trademark but would be very difficult to register, while Redox OS is a unique name that we could register, but may not be worth the extra money for the limited amount of extra protection.

Copyright and trademark are two different ideas. Copyright is the actual image or if you've written paragraphs, the actual content of the text.
The trademark is the use of the image to represent your business or product. Common law would protect us for the trademark, it's a little trickier with the image because we make the image available
sort of as a creative commons kind of a thing. 
We would be protected if somebody was pretending to be Redox and selling t-shirts when in fact none of the donations were coming to us. We would have some legal rights there.

The trademark policy from COSMIC basically states, you can't use COSMIC unless it's officially associated with the project or you have permission and if you are to use the logo to refer to COSMIC then it has to be in a way that doesn't modify the logo.
so it actually supersedes the copyright law for the image, the image could be one that you could modify and have free rights to so long as you're not officially referring to the project,
if you officially refer to the project you have to use the image unedited, and so there is a logo section in the trademark policy that covers that. 

Resolved: We will not pursue trademark registration at this time, and we adapt a policy for the use of the Redox OS name as an unregistered trademark from COSMIC. Those in favor say Aye, those opposed say No.

Note that the budget contains $5000 to cover the cost of trademark registration so that will increase our end of year balance to $20,920.

It was noted that none of us are IP lawyers so if you hear anything in this that doesn't sound right, you can reach out to us.

## Website

We've chosen to stick with the Hugo templating engine for now, because it already exists, but we will reconsider that in the future.

We are hoping that the design of the website will at least for the near future determine the path that Redox will go in visually, whether the COSMIC integration can help us shape the design language of Redox as a whole,

The current state of the website is that it's ready for the English content to be reviewed.
Jacob has provided the visual design and the English content and is waiting for review.
Jeremy recommends that we drop the translations for now and have an English only version and rely on community translations. 
Jacob would like to identify specific langauges that will be maintained, such as German and Portuguese.

## Agenda for next meeting

The proposed agenda for the next meeting is:

Approval of the minutes of this meeting
Report on year to date finances
Report on fundraising activity
Report on trademark
Report on website

The meeting was adjourned at 11:28am MDT.