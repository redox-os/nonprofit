Board Meeting Script 2025Q1

## Preamble

President: Welcome to our first board meeting of 2025.

## Call to order

President: I call the meeting of the Board of Directors of Redox OS to order.

## Attendance

Ron: When I state your name please acknowledge your presence.

- Ron Williams: Present
- Jeremy Soller: Present
- Alberto Souza: Present
- Jacob Schneider: Present

Ron: We have a quorum.

## Minutes from last meeting

Ron: The minutes from the previous board meeting have been provided to you.
Are there any corrections?

Ron: If there are no corrections...

Ron: The minutes are approved as distributed.

## Elections of Directors

Ron: As required by the Bylaws, the Redox OS nonprofit elects its directors and officers each year. Just to clarify, an officer is someone who performs duties on behalf of the board, while a director is a voting member of the board.

Redox officers are also directors, so as we elect our officers, we are also electing them as members of the board. In particular, the president presides over board meetings and the vice-president acts in the president's place if the president cannot attend a board meeting.

I would like to open the floor to nominees for president and director.

Jacob: I nominate Ronald Williams to the office of president and director.

Alberto: Seconded.

Ron: Are there any other nominees for president and director?

Ron: If there are no other nominees...

Ron: All those in favor of myself, Ronald Williams, for president and director, say Aye, those opposed say No.

Jeremy: Aye.

Alberto: Aye.

Jacob: Aye.

Ron: The Ayes have it, yours truly, Ronald Williams, is elected president and director.

Ron: I would like to open the floor to nominees for vice-president and director.

Alberto: I nominate Jeremy Soller to the office of vice-president and director.

Jacob: Seconded.

Ron: Are there any other nominees for vice-president and director?

Ron: If there are no other nominees...

Ron: All those in favor of Jeremy Soller for vice-president and director say Aye, those opposed say No.

Alberto: Aye.

Ron: Aye.

Jacob: Aye.

Ron: The Ayes have it, Jeremy Soller is elected vice-president and director.

Ron: I would like to open the floor to nominees for secretary and director.

Jacob: I nominate Alberto Souza as secretary and director.

Jeremy: Seconded.

Ron: Are there any other nominees for secretary and director?

Ron: If there are no other nominees...

Ron: All those in favor of Alberto Souza for secretary and director say Aye, those opposed say No.

Jacob: Aye.

Jeremy: Aye.

Ron: Aye.

Ron: The Ayes have it, Alberto Souza is elected secretary and director.

Ron: I would like to open the floor to nominees for treasurer.

Jacob: I nominate Jeremy Soller for treasurer.

Alberto: Seconded.

Ron: Are there any other nominees for treasurer?

Ron: If there are no other nominees...

Ron: All those in favor of Jeremy Soller for treasurer say Aye, those opposed say No.

Alberto: Aye.

Jacob: Aye.

Ron: Aye.

Ron: The Ayes have it, Jeremy Soller is elected treasurer.

Ron: I would like to open the floor to nominees for other directors.

Alberto: I nominate Jacob Schneider for director.

Jeremy: Seconded.

Ron: Are there any other nominees for director?

Ron: If there are no other nominees...

Ron: All those in favor of Jacob Schneider for director say Aye, those opposed say No.

Jeremy: Aye.

Alberto: Aye.

Ron: Aye.

Ron: The Ayes have it, Jacob Schneider is elected director.

## This is end of the mandatory part of the meeting

## 2024 Financials

Ron: We do not have the 2024 Financials ready for review yet.
I will work with Jeremy to complete that in the next week or two,
and once the board approves, we will publish that.

## 2025 Budget

Ron: Can I get a motion to discuss the 2025 budget?

Alberto: Moved.

Jacob: Seconded.

Ron: This is a basic operating budget, intended to authorize spending for 2025.
It based on conservative estimates of grants and funding,
and we will have to revise the budget based on a broader development plan and/or major donations.

|  In USD                    |    2025 |
| :------------------------- |  -----: |
| Expenses	                 |         |
| Server Hosting             |   1,500 |
| Community Manager          |   9,600 |
| Misc.                      |     500 |
| Accounting and Filings     |     500 |
| Provision for Taxes        |   5,000 |
| Redox Summer of Code       |  11,000 |
| Other Development          |  15,000 |
| **Expenses Total**         |  43,100 |
|                            |         |
| Income		                 |         |
| Patreon                    |   7,200 |
| Merchandise                |     800 |
| Expected Grants            |  12,000 |
| Other Donations TBD        |         |
| **Income Total**           |  20,000 |
|                            |         |
| **Deficit**                | -23,100 |
|                            |         |
| Net Assets (2024)          |  28,109 |
| **Final Balance**          |   5,009 |

Are there any concerns with the budget as presented?

Can I get a motion to approve the operating budget, and to authorize the treasurer to make expenditures according to the budget?

Jacob: Moved.

Alberto: Seconded.

Ron: It's moved and seconded that the operating budget is approved and the treasurer is authorized to make expenditures according to the budget.
Those in favor say Aye, those opposed say No.

Jacob, Jeremy, Alberto: Aye or No.

Ron: The motion is carried.

## Fundraising

Ron: Can I get a motion to discuss fundraising?

Jacob: Moved.

Alberto: Seconded.

Ron: The most immediate opportunity for grants at this time seems to be NLnet.
I have submitted three proposals, totalling 80,000 Euros.
Depending on the response to these proposals, we may apply for more funding from NLnet in the next few months.
It's my hope that we can fund one or two full-time developers starting in the latter half of this year,
and carrying forward to 2025.

Any development plan for Redox for 2025 at this point has a large dependency on NLnet funding.
I am hoping that we receive some feedback on our proposals in mid-February.
I would like to wait until then before we address the development plan more fully.

We will be applying for Google Summer of Code this year,
and we will continue to try to build relationships with vendors as another possible source of funding.
And we will continue to look for open source grants that we can apply for.

I would like to note that I have requested from the board, and the board has agreed,
that I may include funded work for myself as part of proposals that we submit.

Are there any concerns or questions?

## Action Items

Ron: Can I get a motion to discuss other action items?

Jacob: Moved.

Alberto: Seconded.

Ron: The following action items have been previously identified.
If anyone has anything to report on these items, please let me know.

- Coordinating Contributors - Action: Ron to try to use GitLab Projects.
- Website - Action: Jacob to create an MR and Ron to review.
- Trademark - Action: Jeremy to draft a Redox trademark document based on COSMIC's.

In addition, we have the following new action items.

- 2024 Financials - Action: Ron and Jeremy to complete and review.
- 2025 Development Plan - Action: Ron and Jeremy to complete and review once feedback from NLnet is received.

Are there any other new items?

## Agenda for next meeting

I would like to propose the following agenda for the next board meeting, to take place no later than June 30.

- Approval of the minutes of this meeting
- Report on fundraising
- Report on development
- Report on action items

Ron: Is there any other business to discuss at this time?

Ron: There being no other business, can I get a motion to adjourn?

Alberto: Moved.

Jacob: Seconded.

Ron: It is moved and seconded that we adjourn. Those in favor say Aye, those opposed say No.

Alberto: Aye.

Jacob: Aye.

Jeremy: Aye.

Ron: The Ayes have it, the meeting is adjourned.