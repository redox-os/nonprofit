# Redox OS Board Meeting 2024Q3

The Redox OS Board Meeting for 2024Q3 took place on September 25, 2024 at 11:00am MDT (UTC-6).
A video of the meeting is posted on [the Redox OS YouTube Channel](https://www.youtube.com/watch?v=Dh8CRe-DAFg).

## Attendance

- Ron Williams, president: present
- Jeremy Soller, vice president and treasurer: present
- Alberto Souza, secretary: present
- Jacob Schneider, director: present

## Minutes from last meeting

Resolved: The minutes from the Redox OS Board Meeting 2024Q2 were accepted. The minutes are available in the
[nonproft repository on GitLab](https://gitlab.redox-os.org/redox-os/nonprofit/-/blob/main/meetings/board-meeting-2024Q2/minutes-2024Q2.md?ref_type=heads).

## Finances

Jeremy reported on the Nonprofit finances.

| Account      |      May 31 |      Sep 25 |
| :----------- |  ---------: |  ---------: |
| Bank Account |   28,975.35 |   26,105.06 |
| Credit Card  |  (2,239.63) |  (1,782.95) |
| Teespring    |      172.08 |      409.91 |
| Bitcoin      |       67.50 |       63.63 |
| Ethereum     |       84.50 |      430.70 |
| Radicle/USDC |             |    4,134.69 |
| Radworks     |             |   12,459.08 |
| **Total**    |   27,059.80 |   41,820.12 |

The cryptocurrencies are at their current exchange rate,
but they are not liquid for us right now,
as we don't have a corporate account for them.
This is particularly important for the Radworks amount
as it could have tax implications if it goes through a personal account.
We have applied for an account with Kraken,
but we need to provide additional documentation to complete
the setup of our account.

The 501(c)(4) Nonprofit status is not yet approved.
They requested an additional form, a 1024 A,
and now are asking for 180 days to approve.
This means the non profit may have to pay standard corporate taxes
for the tax year of 2023.
In that case, we would expect an adjustment to the budget.  

A payment was made by NLnet directly to 4lDO2
for the work on the POSIX Signals project.
This is outside the scope of Redox.
However, there are some future payments from NLnet
that will be coming to Redox, for the testing portion of the project.

## Community Moderation

The Redox community adheres to the Rust Code of Conduct. In short, it covers discriminatory behaviour towards an individual or group. This is not limited to verbal or physical abuse or negative or hurtful remarks on preferences, behaviours or opinions.

We aim to make Redox a warm and welcoming community,
but more importantly, a productive one.
Moderators ensure that these guidelines are adhered to by reminding users of them,
as well as sometimes stepping in to diffuse heated situations.
However, moderators are people too and are just as subject to opinion,
biases or otherwise as everyone else.
Therefore, it should go without saying,
that a situation which has been poorly handled,
should be dealt with through respectful communication with the parties involved.
The purpose of moderation is to help the community to stick together.

## Other Topics

The following action items are to identified for follow-up at the next meeting.

- Corporate Governance Document - This is required for us to apply to Kraken 
for a corporate account for exchanging our coins. Action: Ron
- Development budget - Our revenue is good compared to our spend rate.
We need to discuss our development budget and plans for 2025. Action: Ron
- Community contributors - We need to do a better job of coordinating our volunteers.
We also need to lay out more student projects and RSoC-type projects.
And have better visibility of who is doing what progress contributors are making,
without being heavy handed. Action: Jeremy, Ron
- RustConf Trip Report - There were many interesting discussions
and implications coming out of RustConf.
I have some proposals to address some of the topics. Action: Ron
- Website - Jacob has a draft website waiting for Ron's review.
There are some big-picture items that I think need further discussion. Action: Ron, Jacob
- Trademark - We need to write up our trademark claim. Action: Jeremy

## Adjournment

The meeting was adjourned at 11:25am MDT.




