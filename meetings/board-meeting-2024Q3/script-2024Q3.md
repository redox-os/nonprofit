# Board Meeting Script 2024Q3

## Preamble

Ron: Welcome to the Redox OS board meeting for Q3 of 2024.

## Call to order

Ron: I call the meeting of the Board of Directors of Redox OS to order.

## Attendance

Ron: When I state your name please acknowledge your presence.

- Jeremy Soller: Present
- Alberto Souza: Present
- Jacob Schneider: Present
- Ron Williams: Present

Ron: We have a quorum.

Ron: This meeting will be short as I have not been able to prepare very many topics for discussion. There are several things that will need to be discussed and decided among the board, which we will have to do incrementally over the next several weeks, and then we will formally adopt them during the next board meeting.

## Minutes from last meeting

Ron: The minutes from the previous board meeting are available on GitLab, and have been provided to you for review.
Are there any corrections?

Ron: If there are no corrections...

Ron: The minutes are approved as distributed.

## Finances

Ron: Can I get a motion to have the treasurer give an update on our finances?

Alberto: Moved.

Jacob: Seconded.

Ron: Jeremy, could you please provide an update on finances?

Jeremy: Our account balances at the end of last quarter and the end of last month are as follows.

| Account      |      May 31 |      Aug 31 |
| :----------- |  ---------: |  ---------: |
| Bank Account |   28,975.35 |   26,105.06 |
| Credit Card  |  (2,239.63) |  (1,782.95) |
| Teespring    |      172.08 |      409.91 |
| Bitcoin      |       67.50 |       63.63 |
| Ethereum     |       84.50 |      430.70 |
| Radicle/USDC |             |    4,134.69 |
| Radworks     |             |   12,459.08 |
| **Total**    |   27,059.80 |   41,820.12 |

- We are in the process of getting a corporate account for converting our various coins into USD. This is particularly important for the Radworks amount as it could have tax implications if it goes through a personal account.
- The 501(c)(4) Nonprofit status is not yet approved. They requested an additional form, and now are asking for 180 days to approve. This may have some tax implications for 2024 but we don't know at this time.
- A payment was made by NLnet directly to 4lDO2 for the work on the POSIX Signals project. This is outside the scope of Redox. However, there are some future payments from NLnet that will be coming to Redox, for the testing portion of the project.

## Community Moderation

Ron: Can I have a motion to discuss Community Moderation.

Jacob: Moved.

Jeremy: Seconded.

Jacob: In light of recent events within the community, I would like to reiterate some of our communication guidelines for effective and respectful communication.

The Redox community adheres to the Rust Code of Conduct. In short, it covers discriminatory behaviour towards an individual or group. This is not limited to verbal or physical abuse or negative or hurtful remarks on preferences, behaviours or opinions.

We aim to make Redox a warm and welcoming community, but more importantly, a productive one.
Moderators ensure that these guidelines are adhered to by reminding users of them, as well as sometimes stepping in to diffuse heated situations.
However, moderators are people too and are just as subject to opinion, biases or otherwise as everyone else.
Therefore, it should go without saying, that a situation which has been poorly handled, should be dealt with through **respectful** communication with the parties involved.

Ron: Does anyone have any further comments?
If there are no changes to the current policy, can I get a motion to close the discussion.

Jeremy: Moved.

Jacob: Seconded.

Ron: It is moved and seconded that we close the discussion. Those in favor say Aye, those opposed say No.

Jeremy: Aye

Jacob: Aye

Alberto: Aye

## Other Topics

Ron: Because we did not cover as much ground today as I would have liked, I would like to propose some topics to be addressed over the coming weeks, and some corresponding action items. Could I have a motion to propose some topics?

Jeremy: Moved.

Jacob: Seconded.

Ron: I will propose some action items and suggest assignments. If you would like to comment on the items, you can, but please keep it short, as we will be following up separately.

- Corporate Governance Document - This is required for us to apply for a corporate account for exchanging our coins. The goal is to draft something simple that reflects our current structure. Action: Ron
- Development budget - Our revenue is good compared to our spend rate. We need to discuss our development budget and plans for 2025. Action: Ron
- Community contributors - The community is growing rapidly. We need to do a better job of coordinating and focusing our volunteers. We also need to lay out more student projects and RSoC-type projects. And have better visibility of who is doing what and what progress they are making, without being heavy handed. Action: Jeremy, Ron
- RustConf Trip Report - There were many interesting discussions and implications coming out of RustConf. I have some proposals to address some of the topics. Action: Ron
- Website - Jacob has a draft website waiting for Ron's review. There are some big-picture items that I think need further discussion. Action: Ron, Jacob
- Trademark - We decided on how we will move forward with the trademark, but no one owed an action item. Action: Jeremy?

Other items?

The plan is to address these items before the next board meeting and just summarize and confirm.

## Agenda for next meeting

Ron: I would like to propose the following agenda for the next board meeting, to take place no later than December 31.

- Approval of the minutes of this meeting
- Report on year to date finances
- Report on fundraising activity
- Report on the above items

Ron: Is there any other business to discuss at this time?

Ron: There being no other business, can I get a motion to adjourn?

Jeremy: Moved.

Jacob: Seconded.

Ron: It is moved and seconded that we adjourn. Those in favor say Aye, those opposed say No.

Alberto: Aye.

Jacob: Aye.

Jeremy: Aye.

Ron: The Ayes have it, the meeting is adjourned.