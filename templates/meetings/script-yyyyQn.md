# Board Meeting Script Template

## Preamble

President: Welcome to the Redox OS board meeting for **insert quarter number and year**.

## Call to order

President: I call the meeting of the Board of Directors of Redox OS to order.

## Attendance

President: When I state your name please acknowledge your presence.

- President: Yes
- Treasurer: Yes
- Secretary: Yes

President: We have a quorum.

President: **presents the minutes from the previous meeting** (by posting a link or holding up the document)

President: Can I have a motion to adopt?

Treasurer: Seconded.

Secretary: It is moved and seconded that **speak the motion topic/context**. Is there any discussion?

President: Those in favor say "Aye", those opposed say "No".

Aye = In favor.
Opposed = No.

President: speak his vote.
Treasurer: speak his vote.
Secretary: speak his vote.

**RESOLVED**: Upon motion duly moved, seconded and carried, it was resolved **motion context action**.

## Generic motions

Motion Owner: I move that **speak the motion topic/context**.

Some director: Seconded.

President: It is moved and seconded that **speak the motion topic/context**. Is there any discussion?

President: Those in favor say "Aye", those opposed say "No".

Aye = In favor.
Opposed = No.

President: speak his vote.
Treasurer: speak his vote.
Secretary: speak his vote.

**RESOLVED**: Upon motion duly moved, seconded and carried, it was resolved **motion context action**.

## Two-vote round of generic motions

Motion Owner: I move that **speak the motion topic/context**.

Some director: Seconded.

President: It is moved and seconded that **speak the motion topic/context**. Is there any discussion?

President: Those in favor say "Aye", those opposed say "No".

Aye = In favor.
Opposed = No.

President: speak his vote.
Treasurer: speak his vote.
Secretary: speak his vote.

**RESOLVED**: Upon motion duly moved, seconded and carried, it was resolved **motion context action**.

Motion Owner: I move that **speak the motion topic/context**.

Some director: Seconded.

President: It is moved and seconded that **speak the motion topic/context**. Is there any discussion?

President: Those in favor say "Aye", those opposed say "No".

Aye = In favor.
Opposed = No.

President: speak his vote.
Treasurer: speak his vote.
Secretary: speak his vote.

**RESOLVED**: Upon motion duly moved, seconded and carried, it was resolved **motion context action**.

## Report motions

President: I would like to request that **speak the motion report request**.

Responsible director: speak the report.

## Adoption motions

Director motion owner: I would like to present **speak the adoption context**.

First director - Yes/No.
Second director - Yes/No.

Adoption motion owner: As have I, **speak the motion action**.

Some director: Seconded.

President: It is moved and seconded that **speak the motion topic/context**. Is there any discussion?

President: Those in favor say "Aye", those opposed say "No".

Aye = In favor.
Opposed = No.

President: speak his vote.
Treasurer: speak his vote.
Secretary: speak his vote.

**RESOLVED**: Upon motion duly moved, seconded and carried, it was resolved **motion context action**.

## Authorize motions

President: I move to authorize the **speak the motion title/topic**.

Some director: Seconded.

President: It is moved and seconded that **speak the motion topic/context**. Is there any discussion?

President: Those in favor say "Aye", those opposed say "No".

Aye = In favor.
Opposed = No.

President: speak his vote.
Treasurer: speak his vote.
Secretary: speak his vote.

## End

President: Is there any other business for the board to consider at this time?

Discussion.

## Agenda for Next Board Meeting

President: The following items are proposed for the next board meeting, to take place within the next three months.
- write the items

## Adjournment

President: I move to adjourn.

Some director: Seconded.

President: It is moved and seconded that we adjourn. Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - No. Carried.

President: speak his vote.
Treasurer: speak his vote.
Secretary: speak his vote.

This meeting is adjourned.
