+++
title = "Redox OS x.y.z"
author = "Ribbon, Ron Williams and Jeremy Soller"
date = "yyyy-mm-dd"
+++

Redox OS is an Unix-like general-purpose microkernel-based operating system written in Rust.

<!--
Image HTML Code

<a href="/img/screenshot/image-name.png"><img class="img-responsive" alt="Description" src="/img/screenshot/image-name.png"/></a>

-->

## Overview

The version x.y.z is packed with new features, improvements, bug fixes and cleanup. 

We would like to thank all maintainers and contributors whose hard work has made this release possible.

Here are just a few of the highlights!

- Item 1
- Item 2

## Donations and Funding

We are seeking community donations to support more full-time developers, we need the help of generous donors like you!

If you want to help support Redox development, you can make a donation to our [Patreon](https://www.patreon.com/redox_os), [Donorbox](https://donorbox.org/redox-os), or choose one of the other methods in our [Donate](https://redox-os.org/donate/) page.

You can also buy Redox merch (T-shirts, hoodies and mugs!) at our [Redox store](https://redox-os.creator-spring.com/).

If you know an organization or foundation that may be interested in supporting Redox, please contact us on Matrix or the email: donate@redox-os.org

## Overview Video

If you want to avoid the setup work of running Redox on real hardware or in a virtual machine, have a look at our [Software Showcase X]() where we show off programs running on Redox.

## Running Redox

It is recommended to try Redox OS in a virtual machine before trying on real hardware. See
the [supported hardware](https://www.redox-os.org/faq/#which-devices-does-redox-support) section for details on what
hardware to select for the best experience.

- Read [this](https://doc.redox-os.org/book/running-vm.html) page to learn how to run the Redox images in a virtual machine
- Read [this](https://doc.redox-os.org/book/real-hardware.html) page to learn how to run the Redox images on real hardware
- Read [this](https://doc.redox-os.org/book/installing.html) page to learn how to install Redox

### Demo

A 1536 MiB image containing the Orbital desktop environment as well as pre-installed demonstration programs.

- [Real Hardware Image](https://static.redox-os.org/releases/x.y.z/x86_64/redox_demo_x86_64_*_livedisk.iso.zst)
- [Virtual Machine Image](https://static.redox-os.org/releases/x.y.z/x86_64/redox_demo_x86_64_*_harddrive.img.zst)

The demo image includes these additional packages:

- [DOSBox](https://www.dosbox.com/) - A DOS emulator
- Games using PrBoom:
    - DOOM (Shareware)
    - [FreeDOOM](https://freedoom.github.io/)
- [Neverball and Neverputt](https://neverball.org/) - OpenGL games using LLVMPipe (performance may vary!)
- [orbclient](https://gitlab.redox-os.org/redox-os/orbclient) - An Orbital client demo
- [Periodic Table](https://gitlab.redox-os.org/redox-os/periodictable) - A program for viewing information about chemical elements
- [Terminal games](https://gitlab.redox-os.org/redox-os/games) - Command-line games
- [rodioplay](https://gitlab.redox-os.org/redox-os/rodioplay) - A FLAC/WAV music player
- [Sodium](https://gitlab.redox-os.org/redox-os/sodium): A vi-like text editor
- [sopwith](http://www.sopwith.org/): A classic PC air combat game
- syobonaction - A freeware platforming game

### Desktop

A 512 MiB image containing the Orbital desktop environment and some programs for common tasks. Use this if you want to download a smaller image.

- [Real Hardware](https://static.redox-os.org/releases/x.y.z/x86_64/redox_desktop_x86_64_*_livedisk.iso.zst)
- [Virtual Machine Image](https://static.redox-os.org/releases/x.y.z/x86_64/redox_desktop_x86_64_*_harddrive.img.zst)

### Server

A 512 MiB image containing only the command-line environment. Use this if the desktop image is not working well for you.

- [Real Hardware](https://static.redox-os.org/releases/x.y.z/x86_64/redox_server_x86_64_*_livedisk.iso.zst)
- [Virtual Machine Image](https://static.redox-os.org/releases/x.y.z/x86_64/redox_server_x86_64_*_harddrive.img.zst)

## Key Improvements for Release x.y.z

- Item 1
- Item 2

## This Month in Redox

The monthly reports offer more details about the changes present on this release post.

You can read them on the following links:

- [This Month in Redox - (Month) (Year)]()

## Join us on Matrix Chat

If you want to contribute, give feedback or just listen in to the conversation,
join us on [Matrix Chat](https://matrix.to/#/#redox-join:matrix.org).

<!--
## Discussion

Here are some links to discussion about this news post:

- [Fosstodon @redox]()
- [Fosstodon @soller]()
- [Patreon]()
- [Phoronix]()
- [Reddit /r/redox]()
- [Reddit /r/rust]()
- [X/Twitter @redox_os]()
-->

## Changes

There have been quite a lot of changes since x.y.z. We have manually enumerated
what we think is important in this list. Links to exhaustive source-level change
details can be found in the [Changelog](#changelog) section.

## In Depth

The most important changes are shown below.

## Kernel Improvements



## Driver Improvements



## System Improvements



## Relibc Improvements



## Networking Improvements



## Filesystem Improvements



## Programs



## Build System Improvements



## Documentation Improvements



## Changelog

As many changes happened it's not possible to write everything on this post, this section contains all commits since the 0.8.0 version generated by the [changelog](https://gitlab.redox-os.org/redox-os/redox/-/blob/master/changelog.sh) script:

- [redox](https://gitlab.redox-os.org/redox-os/redox/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [cookbook](https://gitlab.redox-os.org/redox-os/cookbook/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [audiod](https://gitlab.redox-os.org/redox-os/audiod/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [bootloader](https://gitlab.redox-os.org/redox-os/bootloader/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [bootstrap](https://gitlab.redox-os.org/redox-os/bootstrap/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [ca-certificates](https://gitlab.redox-os.org/redox-os/ca-certificates/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [contain](https://gitlab.redox-os.org/redox-os/contain/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [coreutils](https://gitlab.redox-os.org/redox-os/coreutils/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [cosmic-edit](https://github.com/pop-os/cosmic-edit)
- [cosmic-files](https://github.com/pop-os/cosmic-files)
- [cosmic-icons](https://github.com/pop-os/cosmic-icons)
- [cosmic-term](https://github.com/pop-os/cosmic-term)
- [curl](https://gitlab.redox-os.org/redox-os/curl/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [drivers](https://gitlab.redox-os.org/redox-os/drivers/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [escalated](https://gitlab.redox-os.org/redox-os/escalated/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [extrautils](https://gitlab.redox-os.org/redox-os/extrautils/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [findutils](https://gitlab.redox-os.org/redox-os/findutils/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [initfs](https://gitlab.redox-os.org/redox-os/redox-initfs/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [installer](https://gitlab.redox-os.org/redox-os/installer/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [installer-gui](https://gitlab.redox-os.org/redox-os/installer-gui)
- [ion](https://gitlab.redox-os.org/redox-os/ion/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [ipcd](https://gitlab.redox-os.org/redox-os/ipcd/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [kernel](https://gitlab.redox-os.org/redox-os/kernel/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [netstack](https://gitlab.redox-os.org/redox-os/netstack/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [netutils](https://gitlab.redox-os.org/redox-os/netutils/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [orbdata](https://gitlab.redox-os.org/redox-os/orbdata/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [orbital](https://gitlab.redox-os.org/redox-os/orbital/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [orbutils](https://gitlab.redox-os.org/redox-os/orbutils/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [pkgutils](https://gitlab.redox-os.org/redox-os/pkgutils/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [pop-icon-theme](https://github.com/pop-os/icon-theme/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [ptyd](https://gitlab.redox-os.org/redox-os/ptyd/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [redoxfs](https://gitlab.redox-os.org/redox-os/redoxfs/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [relibc](https://gitlab.redox-os.org/redox-os/relibc/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [resist](https://gitlab.redox-os.org/redox-os/resist/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [userutils](https://gitlab.redox-os.org/redox-os/userutils/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [init](https://gitlab.redox-os.org/redox-os/init/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [logd](https://gitlab.redox-os.org/redox-os/logd/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [ramfs](https://gitlab.redox-os.org/redox-os/ramfs/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [randd](https://gitlab.redox-os.org/redox-os/randd/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
- [zerod](https://gitlab.redox-os.org/redox-os/zerod/-/compare/first-8-values-current-commit-hash...first-8-values-old-commit-hash)
