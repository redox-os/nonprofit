# Redox OS Nonprofit

The Redox OS nonprofit corporation, incorporated in Colorado, EIN 92-2297602, is
governed by a volunteer board of directors that as of February 14, 2023,
consists of the following voting officers:

- Ron Williams, acting as President, tasked with directing the meetings of the
  organization.
- Alberto Souza, acting as Secretary, tasked with recording the
  meetings of the organization.
- Jeremy Soller, acting as Treasurer, tasked with managing the finances of the
  organization.
- Jacob Schneider, acting as Director, tasked with discussions and voting on the organization.

The following documents are the governing documents of the Redox OS nonprofit
corporation:

- [Bylaws](Bylaws.odt), describing the structure and basic rules of the
  organization.
- [Conflict of Interest Policy](Conflict_of_Interest_Policy.odt), describing the
  actions to take in the event that board members become aware of a conflict of
  interest.

### Repository Layout

- [Acceptances](/acceptances/) - Formal statements of each director accepting his term of office.
- [Meetings](/meetings/) - Board meeting scripts.
- [Board Meeting Script Template](board-meeting-script-template.md) - A temmplate to create new board meeting scripts.
- [Funding](funding.md) - Current funding services.
- [Sponsors](sponsors.md) - Current corporate sponsors and sponsorship programs.

### Term Of Office

- Ron Williams (President) - January, 2024 until January, 2025.
- Jeremy Soller (Treasurer and Vice President) - January, 2024 until January, 2025.
- Alberto Souza (Secretary) - January, 2024 until January, 2025.
- Jacob Schneider (Director) - January, 2024 until January, 2025.

### Timezones

- Ron Williams - UTC-8 (standard time) and UTC-7 (daylight savings time)
- Jeremy Soller - UTC-7 (standard time) and UTC-6 (daylight savings time)
- Alberto Souza - UTC-3
- Jacob Schneider - UTC+1

### Board Meeting Pattern

All board meetings occurs within the four quarters of each year, excluding emergency meetings of course.

### Board Meeting Call

We use the Jitsi Meet technology inside the Board room on Matrix, the minutes are recorded with video and audio.
