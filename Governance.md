# Redox Governance

Redox OS is an open source project, developing an Unix-like microkernel-based operating system written in Rust.
It is community developed under the MIT license, and funded by donations and grants.

Technical direction for the project is under the authority of Jeremy Soller,
the creator of the project and technical lead.
Financial and community matters are managed by the Redox OS Nonprofit.

## Redox OS Nonprofit

The Redox OS Nonprofit, a 501(c)(4) nonprofit incorporated in Colorado, USA, is governed by a board of directors,
whose authority and responsibilities are defined in the
[Redox OS Bylaws](https://gitlab.redox-os.org/redox-os/nonprofit/-/blob/main/Bylaws.odt?ref_type=heads).
The board of directors is responsible for supporting the Redox OS open source project,
its brand, and its community.

In any conflict between this document and the Redox OS Bylaws,
the Bylaws take precedence.

### Board of Directors

The board of directors is elected annually. The current board of directors consists of:

Ron Williams - President and Director since June 21, 2023
Jeremy Soller - Vice President and Director since June 21, 2023
Alberto Souza - Secretary and Director since June 21, 2023
Jacob Schneider - Director since January 23, 2024
Jacob Lorentzon - Non-voting member since June 21, 2023

### Meetings and Decisions of the Board

The board of directors meets at least quarterly and operates on a majority vote principle.
Decisions of the board are recorded in the minutes of the meetings available in the
[Nonprofit GitLab Repository](https://gitlab.redox-os.org/redox-os/nonprofit/-/tree/main/meetings?ref_type=heads).

### Private GitLab Repository

From time to time, the board of directors may choose to restrict access to some information,
such as personal contact information,
or information provided under non-disclosure agreements.
This information may be stored in a private GitLab repository.

### Annual Budget

The annual budget for Redox is determined by the board of directors
and recorded in the minutes of the first board meeting each year.
Adjustments to the budget are made from time to time,
based on actual receipts and expenditures,
and agreed upon by the board.

### Management of Accounts

The treasurer for the nonprofit is Jeremy Soller.
He is responsible for managing the nonprofit accounts,
and is granted authority by the board to make expenditures
according to the budget.

### Fundraising

Ron Williams is responsible for fundraising activities,
including grant proposals, merchandise sales,
and donor relationships.

### Paid Staff

The nonprofit has no full-time staff.

Alberto Souza is paid a monthly stipend for his role of community manager and Redox OS contributor.

### Development Expenditures

The board of directors establishes a budget for development activities.
Jeremy Soller, in his role as technical lead for Redox OS,
determines which developers are compensated and for what work,
within the designated budget.
In his role as Redox OS treasurer,
Jeremy is responsible for development agreements with developers.

The nonprofit is under the legal jurisdiction of the US,
and as such, cannot engage in financial transactions with
parties that are sanctioned by the US government.

## Community Management

The Redox OS community collaborates via Redox OS GitLab and the Redox Matrix space,
which are moderated and managed by the community management team.
Redox OS follows the [Rust Code of Conduct](https://www.rust-lang.org/policies/code-of-conduct).

### Matrix Moderators

Community members must be granted access to the Redox Matrix space in order to participate in discussions.
Under normal circumstances, any person who requests access to the Matrix space is granted access,
unless they have previously been suspended or are reasonably suspected of intent to engage in disruptive behavior.

Moderators can request that members refrain from certain topics, can remove messages,
or can suspend or bar members who violate the code of conduct.

The current Matrix moderators are:

- Alberto Souza
- Jacob Schneider
- Mathew John Roberts
- Ron Williams
- Jeremy Soller


## Technical Decision Making

Jeremy Soller is the technical authority for Redox OS,
sometimes referred to as the Redox [BDFL](https://en.wikipedia.org/wiki/Benevolent_dictator_for_life).
To ensure consistency of vision, design and quality,
Jeremy is the primary reviewer of changes to the Redox OS project.
He may, at his discretion, delegate responsibility for reviewing and merging.

### Requests for Comments

Redox OS follows a Request for Comments process.

For major changes, such as architectural changes, new OS features,
or changes with wide-ranging impact,
users should first propose the idea concept on Matrix,
then if there is some agreement that the idea has merit,
an RFC should be created and presented for review.

RFCs will be reviewed by architecture contributors as appropriate.
Jeremy Soller is the authority for approving RFCs.

### Features Development

For straight-forward changes or localized features,
the change may be submitted as a merge request,
and does not require an RFC.

If several components will be affected, but the change is straight-forward,
an issue may be appropriate to track the changes.
An issue may also be appropriate if some discussion or decision is necessary to complete the work.

### GitLab

The Redox OS GitLab is available for reading and copying by the public,
according to the license terms of the individual repositories and sub-projects.
A small number of repositories are marked private and are not accessible without appropriate credentials.

Technical contributions, documentation changes and issue reporting require a GitLab account.
Community members must register,
request approval through Redox Matrix, and be approved.
Normally, any person that indicates that they have the ability to make a contribution is approved.
Approved accounts have the ability to create or fork projects in their own namespace.

The main Redox OS repositories are in the `redox-os` namespace.
The ability to edit or merge changes into the main Redox repositories
is granted to owners, maintainers and alternate maintainers.
Alternate maintainers do not normally make changes to main repositories
but can do so when circumstances require it.

Other than some specific repositories, the following are the owners and maintainers
of the main repositories in the `redox-os` namespace:

Jeremy Soller - Owner and Maintainer for all repositories
Jacob Lorentzon - Maintainer for development repositories
Alberto Souza - Maintainer for documentation and website repositories, alternate maintainer for development repositories
Ron Williams - Alternate maintainer for all repositories
